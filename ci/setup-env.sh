#!/bin/bash
set -e

usage()
{
	cat << EOF
Usage: $(basename $0)

Configures the current build environment.
EOF
}

main()
{
	echo "Using python from $(which python)"

	# Provision the current python virtual environment
	pip install -r ci/requirements.txt

	# Track real versions to be able to reproduce issues (but don't commit them !)
	pip freeze > ci/requirements.txt

	# Install the conan configuration (profiles, repositories)
	conan config install ci/conan
	conan remote list

	# Authenticate so we're allowed to upload packages on SBRE public conan repository
	# This will use the CONAN_LOGIN_USERNAME_{REMOTE_NAME} and CONAN_PASSWORD_{REMOTE_NAME}
	# environment variables that must be defined on gitlab server side.
	conan user --clean
	conan user --remote sbre-public --password
}

main "$@"
