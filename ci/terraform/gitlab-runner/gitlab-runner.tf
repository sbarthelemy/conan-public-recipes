provider "aws" {
  # This is better to use credentials from external env, like dedicated json file or awscli ocnfigure
  # access_key = "${var.aws_access_key}"
  # secret_key = "${var.aws_secret_key}"
  region     = "eu-west-3"
  version = "~> 1.14"
}

variable "gitlab_registration_token" {}
variable "key_file_path" {}
variable "key_name" {}
variable "public_key" {}
variable "ami_owner" {}
variable "trusted_source_cidr_blocks" {
  default = [
    "158.255.112.194/32" # Paris Office
  ]
}

# Allow to set the number of runner instances from environment
variable "count" {
  default = 1
}

# Tell terraform to store its state into a remote s3 bucket
terraform {
  # The backend config is filled while running terraform init with propers args
  # terraform init -backend=true -backend-config="bucket=sbre-release-terraform-states.softbankrobotics.com" -backend-config="key=aws-gitlab-runners/${DEPLOY_ENV}/terraform.tfstate" -backend-config="region=eu-west-3" -backend-config="encrypt=true"
  backend "s3" {}
}

# Create a VPC to launch our instances into
resource "aws_vpc" "gitlab-runner" {
  cidr_block = "10.0.0.0/16"
  enable_dns_support = true
  enable_dns_hostnames = true
  tags = {
    Name = "gitlab-runner"
    Creator = "terraform"
  }
}

# Create an internet gateway to give our subnet access to the outside world
resource "aws_internet_gateway" "gitlab-runner" {
  vpc_id = "${aws_vpc.gitlab-runner.id}"
}

# Grant the VPC internet access on its main route table
resource "aws_route" "internet_access" {
  route_table_id         = "${aws_vpc.gitlab-runner.main_route_table_id}"
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = "${aws_internet_gateway.gitlab-runner.id}"
}

# Create a subnet to launch our instances into
resource "aws_subnet" "gitlab-runner" {
  vpc_id                  = "${aws_vpc.gitlab-runner.id}"
  cidr_block              = "10.0.1.0/24"
  map_public_ip_on_launch = true
}

# Our gitlab-runner security group to access
# the instances over SSH
resource "aws_security_group" "gitlab-runner" {
  name        = "terraform_gitlab_runner_security_group"
  description = "Used in the terraform for gitlab runners"
  vpc_id      = "${aws_vpc.gitlab-runner.id}"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks ="${var.trusted_source_cidr_blocks}"
  }
  ingress {
    from_port   = 5986
    to_port     = 5986
    protocol    = "tcp"
    cidr_blocks ="${var.trusted_source_cidr_blocks}"
  }
  ingress {
    from_port   = 3389
    to_port     = 3389
    protocol    = "tcp"
    cidr_blocks ="${var.trusted_source_cidr_blocks}"
  }

  # outbound internet access
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_key_pair" "gitlab-runner" {
  # ssh key pair generated with:
  # ssh-keygen -b 4098 -C gitlab-runner@aws -f ~/.ssh/id_rsa.aws.gitlab-runner
  key_name   = "${var.key_name}"
  public_key = "${var.public_key}"
}

data "aws_ami" "gitlabrunner-ubuntu1604" {
  filter {
    name   = "name"
    values = ["gitlabrunner-ubuntu1604*"]
  }

  # You can use the shortcut "self" if the terraform aws account is the same than the ami owner
  #owners     = ["self"]
  owners     = ["${var.ami_owner}"]

  most_recent = true
}

resource "aws_instance" "gitlabrunner-ubuntu1604" {
  connection {
    user     = "ubuntu"
    # Uncomment the line below if you want to use a specific ssh key file instead of putting it in ssh-agent
    #private_key = "${file(var.key_file_path)}"

    # creation-time provisioner find the ssh host by themselves
    # but destroy-time provisioner didn't manage to find it and host remained blank
    # So, we add explicitly the host value below
    host = "${self.public_ip}"
  }

  # If you prefer to specify an explicit ami_id, modify the line below with a static name
  #ami = "ami-b1d766cc"
  ami = "${data.aws_ami.gitlabrunner-ubuntu1604.id}"

  # Allow to specify the number of instances in an external config file (like terraform.tfvars) or from terraform CLI
  count = "${var.count}"

  # Use a t2.micro instance type for terraform behaviour tests, to save cost
  # instance_type = "t2.micro"
  instance_type = "t2.medium"

  vpc_security_group_ids = ["${aws_security_group.gitlab-runner.id}"]
  subnet_id = "${aws_subnet.gitlab-runner.id}"

  # The creation-time declared aws ssh key pair to be able to connect into this instance
  key_name = "${aws_key_pair.gitlab-runner.id}"

  tags = {
    Name = "${format("gitlabrunner-ubuntu1604-%03d", count.index + 1)}"
    Role = "gitlabrunner"
    Creator = "terraform"
  }

  provisioner "remote-exec" {
    # Keep track of the new assigned IP, only to debug purpose
    # On creation time, Register the gitlab runner to gitlab backend
    inline = [
      "sudo gitlab-runner register --non-interactive --config '/etc/gitlab-runner/config.toml' --url https://gitlab.com --run-untagged --executor shell --shell bash --tag-list ubuntu-16.04,linux,cmake,g++,gcc,python2,virtualenv --registration-token ${var.gitlab_registration_token}"
    ]
  }

  provisioner "remote-exec" {
    # On destroy time, Unregister the gitlab runner from gitlab backend
    when = "destroy"
    inline = [
      "sudo gitlab-runner unregister --all-runners"
    ]
  }
}

data "aws_ami" "gitlabrunner-win2016" {
  filter {
    name   = "name"
    values = ["gitlabrunner-win2016*"]
    #values = ["Windows_Server-2016-English-Full-Base-*"]
  }
  owners     = ["self", "amazon"]
  most_recent = true
}

resource "aws_instance" "gitlabrunner-win2016" {
  key_name = "${aws_key_pair.gitlab-runner.id}"
  # retrieve the Administrator password
  get_password_data = true
  connection {
    type = "winrm"
    port = 5986
    host = "${self.public_ip}"
    password = "${rsadecrypt(self.password_data, file("/run/user/1000/id_rsa.aws.gitlab-runner"))}"
    https = true
    insecure = true
  }

  # If you prefer to specify an explicit ami_id, modify the line below with a static name
  ami = "${data.aws_ami.gitlabrunner-win2016.id}"

  #user_data = "${file("../../packer/script/ec2-userdata.ps1")}"
  user_data = "${file("ec2-winrm.ps1")}"
  #user_data = "${file("gitlabregister.userdata")}"
  # Allow to specify the number of instances in an external config file (like terraform.tfvars) or from terraform CLI
  count = "${var.count}"

  # Use a t2.micro instance type for terraform behaviour tests, to save cost
  # instance_type = "t2.micro"
  instance_type = "t2.medium"

  vpc_security_group_ids = ["${aws_security_group.gitlab-runner.id}"]
  subnet_id = "${aws_subnet.gitlab-runner.id}"

  tags = {
    Name = "${format("gitlabrunner-win2016-%03d", count.index + 1)}"
    Role = "gitlabrunner"
    Creator = "terraform"
  }

  # Connection using winrm works but is often (but not always) "reset by peer"
  provisioner "remote-exec" {
    # On creation time, Register the gitlab runner to gitlab backend
    # we disable SSL verification in order to workaround https://gitlab.com/gitlab-org/gitlab-runner/issues/3769 TODO: fix this
    inline = [
      "PowerShell.exe -Command 'Write-Host IT_WORKS'",
      "gitlab-runner install",
      "gitlab-runner register --non-interactive --url https://gitlab.com --executor shell --run-untagged --tag-list win2016,windows,cmake,visual-studio,vs2017,conan,python2,virtualenv --registration-token ${var.gitlab_registration_token} --env GIT_SSL_NO_VERIFY=true",
      "gitlab-runner start"
    ]
  }

  provisioner "remote-exec" {
    # On destroy time, Unregister the gitlab runner from gitlab backend
    when = "destroy"
    inline = [
      "gitlab-runner unregister --all-runners"
    ]
  }
}

data "aws_ami" "gitlabrunner-mini-win2016" {
  filter {
    name   = "name"
    values = ["gitlabrunner-mini-win2016*"]
  }
  owners     = ["self"]
  most_recent = true
}

resource "aws_instance" "gitlabrunner-mini-win2016" {
  key_name = "${aws_key_pair.gitlab-runner.id}"
  # retrieve the Administrator password
  get_password_data = true
  connection {
    type = "winrm"
    port = 5986
    host = "${self.public_ip}"
    # the private key is stored unecrypted in some directory. TODO fix this
    password = "${rsadecrypt(self.password_data, file("/run/user/1000/id_rsa.aws.gitlab-runner"))}"
    https = true
    insecure = true
  }

  # If you prefer to specify an explicit ami_id, modify the line below with a static name
  ami = "${data.aws_ami.gitlabrunner-win2016.id}"

  user_data = "${file("ec2-winrm.ps1")}"
  count = 0

  instance_type = "t2.medium"

  vpc_security_group_ids = ["${aws_security_group.gitlab-runner.id}"]
  subnet_id = "${aws_subnet.gitlab-runner.id}"

  tags = {
    Name = "${format("gitlabrunner-mini-win2016-%03d", count.index + 1)}"
    Creator = "terraform"
  }

  # Connection using winrm works but is often (but not always) "reset by peer"
  provisioner "remote-exec" {
    inline = [
      "PowerShell.exe -Command 'Write-Host IT_WORKS'",
      "gitlab-runner install",
      "gitlab-runner register --non-interactive --url https://gitlab.com --executor shell --run-untagged --tag-list win2016,windows --registration-token ${var.gitlab_registration_token} --env GIT_SSL_NO_VERIFY=true",
      "gitlab-runner start"
    ]
  }
}

