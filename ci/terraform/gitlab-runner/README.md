# Terraform configuration

This terraform configuration, visible in `gitlab-runner.tf` file, is intended to build a full autonomous AWS environment to deploy gitlab runners.
You can update the number of runners by modifying the `count` parameter in the `aws_instance` section.

The new instances will register onto the [softbank conan recipes project](https://gitlab.com/sbarthelemy/conan-public-recipes) at creation time, and will unregister themselves at destroy time.

The terraform state is currently stored remotely into a s3 bucket, which means you do not need to deploy from the same machine each time. If you decide to not use a remote backend for states, you will need to synchronise the terraform.tfstate file by yourself to avoid duplicate the whole environment.

# Install terraform

https://www.terraform.io/intro/getting-started/install.html

Remember to set your AWS credentials (api and secrets keys):
with [awscli](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-welcome.html):
```bash
pip3 install awscli
aws configure
```

or with other methods, like json file or envvars: [Read the packer documentation](https://www.packer.io/docs/builders/amazon.html#specifying-amazon-credentials)

# Build your deployment environment

For AWS gitlab runners environment:

The **first time** you launch terraform in this folder, or if you change the backend type for states:
```bash
cp terraform.tfvars.sample terraform.tfvars
# Update terraform.tfvars.sample file content with your own values

# Explicitly tell which DEPLOY_ENV to use, like dev, qa, prod or myPersonalEnv
# This field is extremely important, as it is used to build the s3 bucket key to store states in the right s3 bucket!
#DEPLOY_ENV=prod
DEPLOY_ENV=YOUR_VALUE_HERE

terraform init \
  -backend=true \
  -backend-config="bucket=sbre-release-terraform-states.softbankrobotics.com" \
  -backend-config="key=aws-gitlab-runners/${DEPLOY_ENV}/terraform.tfstate" \
  -backend-config="region=eu-west-3" \
  -backend-config="encrypt=true"
```

# Day to day commands

To check what execution plan will happen before to launch:

```bash
terraform plan
```

To launch the plan for real: terraform will ask for confirmation before executing:

```bash
terraform apply
```

If you want to shutdown definitively all the environment, use the destroy command:

```bash
# WARN: No way to step backwards!!!
terraform destroy
```
