#! /usr/bin/python

"""
Helper script to ease generation of all the conan packages of a repository.

This makes exporting several recipes at once easier (especially on Windows).
"""

import os
import os.path
import subprocess
import sys


def main():
    """Main entry point."""
    try:
        export_all(sys.argv[1], sys.argv[2])
    except IndexError:
        print(
            'Usage: %s DIRECTORY user/channel\n'
            '\n'
            'Recursively exports the conan recipes located under the '
            'specified directory'
            % sys.argv[0]
        )


def export_all(directory, user_channel):
    """Call conan to export all the recipes from current dir subdirs."""
    for entry in os.listdir(directory):
        if not os.path.isdir(entry):
            continue

        if os.path.isfile(os.path.join(entry, 'conanfile.py')):
            print('\nExporting %s recipe' % entry)
            cmd = 'conan export %s %s' % (entry, user_channel)
            print(cmd)
            subprocess.check_call(cmd.split(' '))


if __name__ == '__main__':
    main()
