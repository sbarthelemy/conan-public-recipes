<powershell>
$shutdownPath = "C:\Windows\WinRMTearDown.ps1"

if ($true) {
  # set-up
  $authBasicValue = (Get-Item WSMan:\localhost\Service\Auth\Basic).Value
  set-Item WSMan:\localhost\Service\Auth\Basic -Value $True
  write-output "adding self-signed certificate"
  $cert = New-SelfSignedCertificate -CertstoreLocation cert:\LocalMachine\My -KeyExportPolicy Exportable -DnsName "packer"
  write-output "adding WinRM HTTPS listener"
  $listener = New-Item -Path WSMan:\LocalHost\Listener -Transport HTTPS -Address * -CertificateThumbPrint $cert.Thumbprint -Force
  write-output "adding firewall rule"
  $ruleName = "WINRM-HTTPS-In-TCP"
  $rule = New-NetFirewallRule -Name $ruleName -DisplayName "Windows Remote Management over HTTPS" -Description "Inbound rule for Windows Remote Management via WS-Management over HTTPS. [TCP 5986]" -Protocol TCP -LocalPort 5986 -Enabled True -Profile Any -Action Allow

  # tear-down
  Set-Content -Path $shutDownPath -Value "# tear down WinRM/HTTPS "
  Add-Content -Path $shutDownPath -Value "`nRemove-NetFirewallRule -Name $ruleName"
  $id = $listener.PSChildName
  Add-Content -Path $shutDownPath -Value "`nRemove-Item -Path WSMan:\Localhost\listener\$id -Recurse"
  $thumb = $cert.Thumbprint
  Add-Content -Path $shutDownPath -Value "`nGet-ChildItem Cert:\LocalMachine\My\$thumb | Remove-Item"
  Add-Content -Path $shutDownPath -Value "`nSet-Item -Key WSMan:\localhost\Service\Auth\Basic -Value $authBasicValue"
}
</powershell>
