Set-PSDebug -Trace 0

# gather and print information about WinRM access
function Write-Introspection () {
  write-output "== Print WinRM-related config ======================================"
  # cfg_a/ Amazon Microsoft Windows Server 2016 Base AMI

  # NetworkCategory might be "Domain", "Private" (network is assumed safe) or "Public" (network is assumed unsafe: disable sharing etc.)
  # On cfg_a, it defaults to "Public"
  Get-NetConnectionProfile -InterfaceAlias Ethernet
  # Most firewall rules are duplicated: one version for the "Public" category, the other for "Private"/"Domain" categories.
  # By default on cfg_a, the firewall
  # * does not allow WinRM/HTTPS connections (TCP 5986)
  # * does allow WinRM/HTTP connections (TCP 5985) (but only from  IPs of the same subnet if profile=="Public")
  $filters = Get-NetFirewallPortFilter
  ForEach ($filter in $filters) {
    if (($filter.Protocol -eq "TCP") -and (($filter.LocalPort -eq 5985) -or ($filter.LocalPort -eq 5986))) {
      write-output "found filter: $filter" $filter
    }
  }
  # By default on cfg_a
  # * WinRM has a HTTP listener, but no HTTPS listener
  # * does not allow basic auth not unencrypted connection (WinRM supports encryption over HTTP, independently of HTTPS)
  #
  # Those defaults seem safe, but do not work with pywinrm & co.
  # Allowing basic auth and unencrypted connection over HTTP would fix pywinrm connectivity, but would be unsafe.
  winrm get winrm/config
  winrm enumerate winrm/config/listener


  ForEach ($listener in Get-Item wsman:\localhost\Listener\Listener*) {
    $id = $listener.PSChildName
    $thumb = (Get-Item -Path "WSMan:\Localhost\listener\$id\CertificateThumbprint").Value
    if ($thumb -ne "") {
      Write-Output "check certificate with thumbprint $thumb"
      Get-ChildItem -Path Cert:\localMachine\My\$thumb | Test-Certificate -AllowUntrustedRoot -Policy SSL
    }
  }

  write-output "===================================================================="
}

write-Introspection
