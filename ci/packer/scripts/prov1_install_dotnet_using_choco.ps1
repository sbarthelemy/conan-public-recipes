# fail on error
$ErrorActionPreference = "Stop"

# Visual Studio 2017 requires dotnet
# Recent dotnet (>=4.6.2) requires a reboot.
#
# from https://chocolatey.org/packages/visualstudio2017buildtools :
#
#   Notes
#   A reboot may be required after (or even before) installing/uninstalling this package.
#   If control over reboots is required, it is advisable to install the dependencies
#   (esp. dotnet4.6.2 or later) first, perform a reboot if necessary,
#   and then install this package.
#
# if choco return 3010, a reboot is needed.
# We don't check the value and reboot from packer anyway.
choco install dotnetfx -y
