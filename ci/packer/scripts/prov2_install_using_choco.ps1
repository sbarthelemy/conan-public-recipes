# fail on error
$ErrorActionPreference = "Stop"

# install stuff using chocolatey
choco install gitlab-runner -y
choco install git -y
choco install cmake -y --installargs "ADD_CMAKE_TO_PATH=System"
# install python and pip.
# python and pip will be put in PATH, but (as usual)
# will not be available in the current powershell invocation.
choco install python2 -y

# install Visual Studio compiler, but not the IDE
choco install visualstudio2017buildtools --params "--add Microsoft.VisualStudio.Workload.VCTools --add Microsoft.VisualStudio.Component.VC.140 --add Microsoft.Component.VC.Runtime.UCRTSDK --add Microsoft.VisualStudio.Component.Windows10SDK.16299.Desktop --wait --passive" -y

# Opt-out from Visual Studio Customer Experience Improvement Program
# It collects various statistics:
# https://docs.microsoft.com/en-us/visualstudio/ide/visual-studio-experience-improvement-program?view=vs-2017
#
# Luis saw it taking up to 50% CPU while trying to perform an actual build,
# leading to the build failing on timeout.
$registryPath = "HKLM:\SOFTWARE\Wow6432Node\Microsoft\VSCommon\15.0\SQM"
IF(!(Test-Path $registryPath))
{
  New-Item -Path $registryPath -Force | Out-Null
}
New-ItemProperty -Path $registryPath -Name "OptIn"  -Value "0" -PropertyType DWORD -Force | Out-Null
