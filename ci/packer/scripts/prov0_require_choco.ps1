# fail on error
$ErrorActionPreference = "Stop"

# install chocolatey if needed.
# inspired from https://github.com/ansible/ansible-modules-extras/blob/devel/windows/win_chocolatey.ps1
$ChocoAlreadyInstalled = get-command choco -ErrorAction 0
if ($ChocoAlreadyInstalled -eq $null)
{
  # install chocolatey
  Write-Output "installing chocolatey..."
  $install_output = (new-object net.webclient).DownloadString("https://chocolatey.org/install.ps1") | powershell -
  Write-Output "$install_output"
  if ($LASTEXITCODE -ne 0)
  {
    throw "error installing chocolatey"
  }
  # choco will be put in PATH, but (as usual) will not be available in the current powershell invocation.
}
