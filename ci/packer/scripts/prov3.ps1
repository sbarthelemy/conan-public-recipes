# fail on error
$ErrorActionPreference = "Stop"

# Remove MAX_PATH=260 paths limit. This is required for example to uncompress boost source
$registryPath = "HKLM:\SYSTEM\CurrentControlSet\Control\FileSystem"
if(!(Test-Path $registryPath))
{
  New-Item -Path $registryPath -Force | Out-Null
}
New-ItemProperty -Path $registryPath -Name " LongPathsEnabled"  -Value "1" -PropertyType DWORD -Force | Out-Null
