# Install  packer

https://www.packer.io/intro/getting-started/install.html

Remember to set your AWS credentials (api and secrets keys):
with [awscli](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-welcome.html):
```bash
pip3 install awscli
aws configure
```

or with other methods, like json file or envvars: [Read the packer documentation](https://www.packer.io/docs/builders/amazon.html#specifying-amazon-credentials)

# Build your image
For AWS gitlab runner based on ubuntu and shell executor:
```bash
packer build gitlabrunner-ubuntu1604.json

```

*Note:* Remember to refer to the [packer documentation](https://www.packer.io/docs/builders/amazon.html#specifying-amazon-credentials) to know how to use your credentials


# Images

- gitlabrunner-{ubuntu1604,win2016}: runners with build tools (compilers) preinstalled
- gitlabrunner-mini-win2016: smaller runner, without build tools, used for quicker packer/terraform
- test-win2016: test-only template used to ensure that the AMIs we build can be used as a template basis.

# Raw data

* on 2019/10/11, I successfully built a gitlabrunner-win2016 image on AWS with
  Packer version 1.4.4. It took 55 minutes to complete.
