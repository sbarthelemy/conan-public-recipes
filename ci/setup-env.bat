REM Provision the current python virtual environment
pip install -r ci\requirements.txt

REM Track real versions to be able to reproduce issues (but don't commit them !)
pip freeze > ci\requirements.txt

REM Install the conan configuration (profiles, repositories)
conan config install ci\conan
conan remote list

REM Authenticate so we're allowed to upload packages on SBRE public conan repository
REM This will use the CONAN_LOGIN_USERNAME_{REMOTE_NAME} and CONAN_PASSWORD_{REMOTE_NAME}
REM environment variables that must be defined on gitlab server side.
conan user --clean
conan user --remote sbre-public --password

