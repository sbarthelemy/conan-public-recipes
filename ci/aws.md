# Amazon Web Services

We use

* Amazon S3 to store terraform's state.
* Amazon EC2 VM instances as gitlab runners,
* Amazon EC2 AMIs as base images for those instances

https://docs.aws.amazon.com/ec2/index.html

## Accounts

We have one AWS account, which scopes everything we do.
Since the bill is per account, it is easy to know how much is spent.

Each developer has its own "AIM User" attached to the account.
Each AIM User has its own set of security credentials
(password, access keys) to log into the AWS console and use the AWS APIs.

An AIM User can also endorse several "Roles", but we don't use roles.

## Instance creation

An instance is created from an AMI and a "key pair", to run on a (virtualized)
hardware (instance type).

Those "key pairs" are independant from the AIM User credentials.
AWS only keeps the public part.

When a linux instance is created, the public key is added to SSH's
`authorized_keys`. Anyone with the private key will be able to log in
through SSH.

When a Windows instance is created, a unique password is created for
the Administrator user, and AWS has a button (and an API) to retrieve
it, encrypted with the public key.
Anyone with the private key will be able the decrypt the password
and log in through RDP.

This only works if the specific AMIs were properly designed to cooperate, doing
special setup at first boot.

For instance,

* on linux, the instance cooperates through "cloud-init"
  https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/amazon-linux-ami-basics.html#amazon-linux-cloud-init
* on recent Windows, the instance cooperates through EC2Launch and sysprep
  https://docs.aws.amazon.com/AWSEC2/latest/WindowsGuide/ec2launch.html#ec2launch-sysprep
  https://docs.microsoft.com/en-us/windows-hardware/manufacture/desktop/sysprep--generalize--a-windows-installation

When creating custom AMIs, it is good to ensure that behave like official
Amazon AMIs with respect to authentication.

## Provisioning

An instance, created from an AMI, often needs to be adapted/customized to its
specific role. This stage is called "provisioning".

Provisioning can involve software installation, configuration, setup of
specific tokens...

There are several way to do provisioning:

* user data:
  Very useful but not encrypted, do not store passwords there.
  Often used for bootstraping (eg. to enable WinRM).
  https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/user-data.html
  https://docs.aws.amazon.com/AWSEC2/latest/WindowsGuide/ec2-windows-user-data.html
* user metadata:
  sometimes used to tell a multi-purpose AMIs what specific purpose it is for.
* SSH on linux
* RDP on Windows (enabled by default but not practical for automation)
* WinRM on Windows (designed for automation, but not usable by default on Amazon AMIs).
* AWS Systems Manager (preinstalled on Amazon AMIs)
  https://docs.aws.amazon.com/systems-manager/latest/userguide/systems-manager-setting-up.html
* Future versions of Windows server might come with an SSH server built-in.

## Software updates

Once an instance is started, it is our responsability to keep it up to date
with security patches.

There are 2 approaches

* keep each instance up to date automatically (eg. using debian's
  unattended-upgrades)
* throw away the outdated instances, and create new ones from up to date AMIs.
  This approach is known as "immutable infrastructure".

## gitlabrunnner images

Packer is used to create gitlabrunner AMIs. Those are official base AMIs from Amazon
and Canonical on which we install build tools (compiler, conan, ...) and the
gitlab-runner client.

Those AMIs do not contain any secret, and are safe to share.

When instances are created from those AMIs, credential initialization takes place
as for official Amazon images.
The user is responsible for calling gitlab=runner register.

## gitlabrunnner instances

Instances are created from the AMIs using terraform.
There is a provisioning step in which the (preinstalled) gitlab-runner service
is configured (with security tokens) and started.

## Security

We received the following guidelines

* at security group (subnet firewall) level, disable all inbound connections, except from
  our office IPs.
* Ensure all connections to/from the VPC are encrypted.

### Security groups

* there is one security group for terraform-deployed gitlab runners
* packer creates temporary security groups when needed

### Protocols

We use

* TCP 22: SSH
* TCP 5986: WinRM over HTTPS
* TCP 3389: RDP (uses SSL)

# tips

## Getting password from Amazon console

We can connect using RDP, following this documentation
https://docs.aws.amazon.com/AWSEC2/latest/WindowsGuide/connecting_to_windows_instance.html

We'll need to provide the private key to decrypt the password.
If the key is stored on disk with a passphrase it'll need to be decrypted,
either to a file

```bash
openssl rsa -in ~/.ssh/id_rsa.aws.gitlab-runner -out  $XDG_RUNTIME_DIR/id_rsa.aws.gitlab-runner
```
then use RDP or WinRM

## Debug ec2launch & user data

there are logs at

- `C:\ProgramData\Amazon\EC2-Windows\Launch\Log\Ec2Launch.log`
- `C:\ProgramData\Amazon\EC2-Windows\Launch\Log\UserdataExecution.log`

## Debugging a Windows packer build

### run packer with the `-debug` flag

and continue (press enter) until it connects through WinRM.

```bash
$ cd $SRC/ci/packer
$ packer build  -debug gitlabrunner-win2016.json
Debug mode enabled. Builds will not be parallelized.
amazon-ebs output will be in this color.

==> amazon-ebs: Prevalidating AMI Name: gitlab-runner-windows 1543423031
==> amazon-ebs: Pausing after run of step 'StepPreValidate'. Press enter to continue.
    amazon-ebs: Found Image ID: ami-00579b9eb9b5353dc
==> amazon-ebs: Pausing after run of step 'StepSourceAMIInfo'. Press enter to continue.
==> amazon-ebs: Creating temporary keypair: packer_5bfec437-68ce-2e82-b7b9-a5f77fbcd4b2
    amazon-ebs: Saving key for debug purposes: ec2_amazon-ebs.pem
==> amazon-ebs: Pausing after run of step 'StepKeyPair'. Press enter to continue.
==> amazon-ebs: Creating temporary security group for this instance: packer_5bfec43d-0fe1-88ea-8ae1-69b6a7c2bb4d
==> amazon-ebs: Authorizing access to port 5986 from 0.0.0.0/0 in the temporary security group...
==> amazon-ebs: Pausing after run of step 'StepSecurityGroup'. Press enter to continue.
==> amazon-ebs: Pausing after run of step 'stepCleanupVolumes'. Press enter to continue.
==> amazon-ebs: Launching a source AWS instance...
==> amazon-ebs: Adding tags to source instance
    amazon-ebs: Adding tag: "Name": "Packer Builder"
    amazon-ebs: Instance ID: i-06f03151b4c151b05
==> amazon-ebs: Waiting for instance (i-06f03151b4c151b05) to become ready...
    amazon-ebs: Public DNS: ec2-35-180-169-96.eu-west-3.compute.amazonaws.com
    amazon-ebs: Public IP: 35.180.169.96
    amazon-ebs: Private IP: 172.31.34.62
==> amazon-ebs: Pausing after run of step 'StepRunSourceInstance'. Press enter to continue.
==> amazon-ebs: Waiting for auto-generated password for instance...
    amazon-ebs: It is normal for this process to take up to 15 minutes,
    amazon-ebs: but it usually takes around 5. Please wait.
    amazon-ebs:
    amazon-ebs: Password retrieved!
    amazon-ebs: Password (since debug is enabled): mtv%TKnyMRlaoAwyw5M=@I&J3)!*RI$C
==> amazon-ebs: Pausing after run of step 'StepGetPassword'. Press enter to continue.
==> amazon-ebs: Waiting for WinRM to become available...
    amazon-ebs: WinRM connected.
    amazon-ebs: #< CLIXML
    amazon-ebs: <Objs Version="1.1.0.1" xmlns="http://schemas.microsoft.com/powershell/2004/04"><Obj S="progress" RefId="0"><TN RefId="0"><T>System.Management.Automation.PSCustomObject</T><T>System.Object</T></TN><MS><I64 N="SourceId">1</I64><PR N="Record"><AV>Preparing modules for first use.</AV><AI>0</AI><Nil /><PI>-1</PI><PC>-1</PC><T>Completed</T><SR>-1</SR><SD> </SD></PR></MS></Obj><Obj S="progress"
RefId="1"><TNRef RefId="0" /><MS><I64 N="SourceId">1</I64><PR N="Record"><AV>Preparing modules for first use.</AV><AI>0</AI><Nil /><PI>-1</PI><PC>-1</PC><T>Completed</T><SR>-1</SR><SD> </SD></PR></MS></Obj></Objs>
==> amazon-ebs: Connected to WinRM!
==> amazon-ebs: Pausing after run of step 'StepConnect'. Press enter to continue.
```

### lookup the data

find

* the public IP (here `35.180.180.96`)
* the password (here `mtv%TKnyMRlaoAwyw5M=@I&J3)!*RI$C`)
* and the name of the temporary security group (here `packer_5bfec43d-0fe1-88ea-8ae1-69b6a7c2bb4d`)
  in the output.

### Connect to the instance

We can now connect either through WinRM+HTTPS or through RDP,
using various software.

#### Connect through RDP

connect to AWS console, find the temporary security group
and allow inbound connection on port 3389, for RDP.

Then connect through RDP, mounting the scripts directory (see above)

Then on the Windows host (from the remote desktop window), run the provisioning scripts manually:

* start powershell (Logo+r, then PowerShell)
* check the mount works `ls \\tsclient\scripts`
* run the script `\\tsclient\scripts\prov0_install_choco.ps1`

Remember to restart powershell after running each script, otherwise the `PATH` is not updated.

##### with freerdp

with freerdp: run

```bash
sudo apt install freerdp2-x11
xfreerdp /u:"Administrator" /p:'tv%TKnyMRlaoAwyw5M=@I&J3)!*RI$C' /drive:scripts,"$SRC/ci/packer/scripts" /v:35.180.169.96
```

##### with rdesktop

run

```bash
sudo apt install rdesktop
rdesktop -g 1000x990 -rdisk:scripts="$SRC/ci/packer/scripts" -u Administrator -p 'mtv%TKnyMRlaoAwyw5M=@I&J3)!*RI$C' 35.180.169.96
```

It used to work, but the last time I tried, it failed with

```
Autoselected keyboard map en-us
ERROR: CredSSP: Initialize failed, do you have correct kerberos tgt initialized ?
Failed to connect, CredSSP required by server.
```
but worked with freerdp :-/.

#### Connect through WinRM

WinRM is an HTTP-based protocol.
Connectivity can be checked using a web browser by opening the url `https://35.180.180.96:5986/wsman`.

##### Connect using pywinrm

install pywinrm for python2 (the python3 version does not work):
```bash
pip install --user pywinrm
```
then run this python script
```python
import winrm
s = winrm.Session('https://35.180.180.96:5986',
                  auth=('Administrator', 'mtv%TKnyMRlaoAwyw5M=@I&J3)!*RI$C'),
                  server_cert_validation='ignore')
r = s.run_ps('(Get-WindowsFeature).Where{$PSItem.Installed}')
print(r.std_out)
```

##### Connect using pypsrp (a python implementation of PowerShell Remoting Protocol)

Install pspsrp
```bash
pip3 install --user pypsrp
```

```python
from pypsrp.client import Client

# this takes in the same kwargs as the WSMan object
client = Client("35.180.180.96",
		username="Administrator",
		password="mtv%TKnyMRlaoAwyw5M=@I&J3)!*RI$C",
		ssl=True, cert_validation=False)

# execute a cmd command
stdout, stderr, rc = client.execute_cmd("dir")
print(std_out)
```

## WinRM, packer and reusing self-signed certificate

To use WinRM over HTTPS one should create a self-signed certificate.
If the instance is used by packer to create a new AMI, one should ensure that the
certificate is removed before creating the AMI.
This has several advantages:

* instances created from the new AMI will not share a certificate: better security

* instances created from the new AMI will not use a certificate whose end of life date is unknown: less state

* reusing a certificate might not work for another instance with a different hostname

If one needs to restart WinRM (or the instance), one should also avoid having WinRM reachable
before the restart, otherwise packer might start using it, then be interrupted
(source: https://hodgkins.io/best-practices-with-packer-and-windows).

### Example of failure when reusing a self-signed certificate

pywinrm fails with:
```
requests.exceptions.ConnectionError: ('Connection aborted.', error(104, 'Connection reset by peer'))
```
Connecting to the Windows computer with RDP and looking at the Event Viewer, one sees

* under `Event Viewer > Windows Logs > Security`

  * EventID: 5061
    Source: Microsoft Windows security
    msg:
    |Cryptographic operation.
    |Subject:
    |  Security ID:        SYSTEM
    |  Account Name:       EC2AMAZ-RKJHMN7$
    |  Account Domain:     WORKGROUP
    |  Logon ID:       0x3E7
    |Cryptographic Parameters:
    |  Provider Name:  Microsoft Software Key Storage Provider
    |  Algorithm Name: UNKNOWN
    |  Key Name:   te-0980e24d-a919-4b0b-9ea1-7b15e0f99928
    |  Key Type:   Machine key.
    |Cryptographic Operation:
    |  Operation:  Open Key.
    |  Return Code:    0x80090016

* under `Event Viewer > Windows Logs > System`

  * EventID: 36870
    Source: Schannel
    msg: A fatal error occurred when attempting to access the TLS server credential private key. The error code returned from the cryptographic module is 0x8009030D. The internal error state is 10001.
  * EventID: 15021
    Source: HttpEvent
    msg: An error occurred while using SSL configuration for endpoint 0.0.0.0:5986.  The error status code is contained within the returned data.

Those issues do not occur if the certificate is generated on the instance it is used.

## Random links

https://github.com/ricardclau/packer-demos/blob/master/templates/windows-server-2012/aws-win2012R2-servicebox.json
http://blog.practicaltech.ca/2018/05/packer-winrm-amis-making-it-work.html
https://david-obrien.net/2016/12/packer-and-aws-windows-server-2016/
https://operator-error.com/2018/04/16/windows-amis-with-even/

https://github.com/hashicorp/terraform/issues/17829
401 Error with WinRM Connection with NTLM

https://github.com/hashicorp/terraform/issues/17859
Support PowerShell Remoting in remote-exec and file provisioners

https://github.com/hashicorp/terraform/issues/16007
WinRM with domain accounts does not work

https://github.com/terraform-providers/terraform-provider-aws/issues/1254
Setting the administrator password for Windows instance

https://github.com/hashicorp/terraform/issues/13083
Terraform provisioner does not yet support bastion connection for winrm

https://www.2ndwatch.com/blog/automating-windows-server-2016-builds-with-packer/
Automating Windows Server 2016 Builds with Packer

https://blog.petegoo.com/2016/05/10/packer-aws-windows/
