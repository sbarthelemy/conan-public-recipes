# This file should only reference toplevel dependencies, and be kept as minimal
# as possible. It will be overwritten by pip freeze, but only to make it
# available as a downloadable artifact that can be used to reproduce the exact
# test environment.

conan

# FIXME: qibuild doesn't handle cmake >= 3.12.0 for now
# See https://gitlab.aldebaran.lan/qi/qibuild/issues/32
# However boost 1.68 detection really appeared in CMake 3.12
cmake==3.12

# Follow closely qibuild by using development version
git+https://github.com/aldebaran/qibuild.git@master
