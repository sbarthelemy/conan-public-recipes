import os.path

from conans import ConanFile, tools


class PthreadsWin32Conan(ConanFile):
    name = 'pthreads-win32'
    version = '2.9.1'
    license = 'LGPL-2.1-only'
    url = 'https://gitlab.com/sbarthelemy/conan-public-recipes'
    description = 'A POSIX threads library for Microsoft Windows'
    settings = 'os', 'compiler', 'build_type', 'arch'
    options = {
        'shared': [True, False],
        'inlined': [True, False],
        'exceptions': [None, 'Structured', 'C++'],
    }
    default_options = (
        'shared=True',
        'inlined=False',
        'exceptions=None',
    )
    generators = 'txt'

    patch_dir = 'patches'
    exports_sources = os.path.join(patch_dir, '*.patch')
    archive_name = 'pthreads-w32-{version}-release.tar.gz'.format(
        version=version.replace('.', '-')
    )
    source_subdir = archive_name.split('.')[0]

    def source(self):
        tools.ftp_download(
            'sourceware.org',
            'pub/pthreads-win32/{filename}'.format(filename=self.archive_name)
        )
        tools.unzip(self.archive_name)

        # Apply patches
        patch_names = [
            '0001-Fix-timespec-redefinition.patch',
            '0002-Don-t-fail-on-static-build.patch',
        ]

        for patch_name in patch_names:
            patch = os.path.join(self.patch_dir, patch_name)
            tools.patch(base_path=self.source_subdir, patch_file=patch)

    def build(self):
        # Patch the Makefile to install at the right location
        source_abspath = os.path.abspath(
            os.path.join(self.build_folder, self.source_subdir)
        )
        tools.replace_in_file(
            os.path.join(self.source_subdir, 'Makefile'),
            'DEVROOT	= C:\\pthreads',
            'DEVROOT    = {path}'.format(path=self.build_folder)
        )

        # Select build target
        compiler = {
            'Visual Studio': 'V',
            'gcc': 'G',
        }

        exceptions = {
            'Structured': 'SE',
            'C++': 'CE',
            'None': 'C',
        }

        shared = {
            'True': '',
            'False': '-static'
        }

        inlined = {
            'True': '-inlined',
            'False': ''
        }

        build_type = {
            'Debug': '-debug',
            'Release': '',
        }

        target = '{compiler}{exceptions}{shared}{inlined}{debug}'.format(
            compiler=compiler[self.settings.compiler.value],
            exceptions=exceptions[self.options.exceptions.value],
            shared=shared[self.options.shared.value],
            inlined=inlined[self.options.inlined.value],
            debug=build_type[self.settings.build_type.value],
        )

        # Build target
        os.chdir(source_abspath)
        self.output.info('Building target %s' % target)
        self.run('nmake clean {target}'.format(target=target))

        # Install
        for dirname in ['dll', 'lib', 'include']:
            os.makedirs(os.path.join(self.build_folder, dirname))

        self.run('nmake install')

    def package(self):
        self.copy('*.h', dst='include', src='include', keep_path=True)
        self.copy('*.dll', dst='bin', src='dll', keep_path=False)
        self.copy('*.lib', dst='lib', src='lib', keep_path=False)

    def package_info(self):
        import glob
        libs = glob.glob(os.path.join(self.package_folder, 'lib', '*'))
        (library_name, _) = os.path.splitext(os.path.basename(libs[0]))
        self.cpp_info.libs = [library_name]
        if self.settings.compiler == 'Visual Studio':
            self.cpp_info.defines = ['HAVE_STRUCT_TIMESPEC']
