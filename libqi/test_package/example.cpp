#include <boost/make_shared.hpp>
#include <qi/log.hpp>
#include <qi/applicationsession.hpp>
#include <qi/anyobject.hpp>

qiLogCategory("myapplication");

class MyService
{
public:
  void myFunction(int val) {
    qiLogInfo() << "myFunction called with " << val;
  }
  qi::Signal<int> eventTriggered;
  qi::Property<float> angle;
};

// register the service to the type-system
QI_REGISTER_OBJECT(MyService, myFunction, eventTriggered, angle);

void print()
{
  qiLogInfo() << "print was called";
}

int main(int argc, char* argv[])
{
  qi::ApplicationSession app(argc, argv);

  // connect the session included in the app
  app.startSession();

  qi::SessionPtr session = app.session();

  // register our service
  session->registerService("MyService", boost::make_shared<MyService>());

  // get our service through the middleware
  qi::AnyObject obj = session->service("MyService");

  // call myFunction
  obj.call<void>("myFunction", 42);

  // call print in 2 seconds
  qi::asyncDelay(&print, qi::Seconds(2));

  // block until ctrl-c
  // app.run();
}

