from conans import ConanFile, CMake, tools


class LibQiConan(ConanFile):
    name = "libqi"
    version = "1.6.3"
    license = "BSD-3-Clause"
    url = "https://gitlab.com/sbarthelemy/conan-public-recipes"
    description = "Package for the libqi library"
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False]}
    default_options = "shared=False"
    generators = "cmake"
    scm = {
        'type': 'git',
        'url': 'https://github.com/liberforce/libqi.git',
        'subfolder': 'libqi',
        'revision': 'ng/conan',
    }
    requires = (
        'boost/1.68.0@conan/stable',
        'gtest/1.8.0@bincrafters/stable',
        'OpenSSL/1.1.0g@conan/stable',
        'zlib/1.2.11@conan/stable',
    )
    # FIXME: build_requires = 'qibuild'

    def source(self):
        # This small hack might be useful to guarantee proper /MT /MD linkage
        # in MSVC if the packaged project doesn't have variables to set it
        # properly
        tools.replace_in_file(
            'libqi/CMakeLists.txt',
            'project(LibQi)',
            (
                'project(LibQi)\n'
                'include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)\n'
                'conan_basic_setup()\n'
            )
        )

    def requirements(self):
        if self.settings.os == 'Windows':
            self.requires('pthreads-win32/2.9.1@sbre/testing')

    def build(self):
        cmake = CMake(self)
        cmake.configure(source_folder="libqi")
        cmake.build()

        # FIXME: For now, we allow building packages even if some unit tests
        # fail, so that it doesn't prevent building the packages that depend
        # on it. This is because some tests may fail for now while we try to
        # get things working on all platforms and configurations.
        if self.should_test:
            ignore_failed_tests = True
            cmake_to_json_cli = (
                # Use a semicolon to separate instructions as they must be sent
                # as a single line
                'import qibuild.project; '
                "qibuild.project.convert_qitest_cmake_to_json('./qitest.cmake', './sdk/qitest.json')"
            )
            self.run('python -c "{}"'.format(cmake_to_json_cli))
            self.run('qitest run --qitest-json ./sdk/qitest.json', ignore_errors=ignore_failed_tests)

        cmake.install()

    def imports(self):
        self.copy("*.dll", dst="sdk/bin", src="bin")
        self.copy("*.dylib*", dst="sdk/bin", src="lib")
        self.copy('*.so*', dst='sdk/bin', src='lib')

    def package(self):
        self.copy('package.xml', src='libqi')

        # Note: The rest of the files to package has already been copied to the
        # right place by calling cmake.install() in the build step

    def package_info(self):
        self.cpp_info.libs = ["qi"]
        # Required to avoid double inclusion of winsock.h in boost
        if self.settings.os == 'Windows':
            self.cpp_info.defines = ['WIN32_LEAN_AND_MEAN']
