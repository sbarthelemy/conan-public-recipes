import os.path

from conans import ConanFile, CMake, tools


class QiLangConan(ConanFile):
    name = "qilang"
    version = "0.1"
    license = "BSD-3-Clause"
    url = "https://gitlab.com/sbarthelemy/conan-public-recipes"
    description = (
        'qilang is a project to generate strongly-typed proxies in client'
        ' code when using the qi library.'
    )
    settings = "os", "compiler", "build_type", "arch", "cppstd"
    options = {
        'shared': [True, False],
        'with_tests': [True, False],
    }
    default_options = (
        'shared=False',
        'with_tests=True',
    )
    generators = "cmake"
    scm = {
        'type': 'git',
        'url': 'https://github.com/aldebaran/qilang.git',
        'subfolder': 'qilang',
        'revision': 'team/platform/dev',
    }
    requires = (
        'libqi/1.6.3@sbre/testing',
        'boost/1.68.0@conan/stable',
        'gtest/1.8.0@bincrafters/stable',
    )
#    FIXME: build_requires
#    build_requires = (
#        'qibuild',
#        'cmake',
#    )
    patch_dir = 'patches'
    exports_sources = os.path.join(patch_dir, '*.patch')

    def source(self):
        # Apply patches
        patch_names = [
        ]
        for patch_name in patch_names:
            patch = os.path.join(self.patch_dir, patch_name)
            tools.patch(base_path='qilang', patch_file=patch)

        # This might be useful to guarantee proper /MT /MD linkage in MSVC
        # if the packaged project doesn't have variables to set it properly
        tools.replace_in_file(
            'qilang/CMakeLists.txt',
            'project(QiLang)',
            (
                'project(QiLang)\n'
                'include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)\n'
                'conan_basic_setup()\n'
            )
        )

    def build_requirements(self):
        if self.settings.compiler == 'Visual Studio':
            self.build_requires('msys2_installer/latest@bincrafters/stable')
        else:
            self.build_requires('bison/3.0.4@bincrafters/stable')
            self.build_requires('flex/2.6.4@bincrafters/stable')

    def configure(self):
        # Won't build unless we force the C++ standard
        if self.settings.compiler != 'Visual Studio':
            self.settings.cppstd = 'gnu11'

    def imports(self):
        self.copy('*.dll', 'sdk/bin', 'bin')
        self.copy('*.dylib', 'sdk/lib', 'lib')

    def build(self):
        cmake = CMake(self, parallel=False)
        on_off = {'True': 'ON', 'False': 'OFF'}
        cmake.definitions['QI_WITH_TESTS'] = on_off[self.options.with_tests.value]

        if cmake.is_multi_configuration:
            # Workaround qibuild not handling Visual Studio multi-config model
            cmake.definitions['CMAKE_BUILD_TYPE'] = self.settings.build_type

        cmake.configure(source_folder="qilang")
        cmake.build()
        cmake.install()

    def package_info(self):
        self.cpp_info.libs = ["qilang"]
