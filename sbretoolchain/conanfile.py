from conans import ConanFile

class SBREToolchainConan(ConanFile):
    name = "sbretoolchain"
    version = "2.9.0"
    license = "<Put the package license here>"
    url = "<Package recipe repository url here, for issues about the package>"
    description = "<Description of Toto here>"
    settings = "os", "compiler", "build_type", "arch"
    options = {
        "shared": [True, False],
        "gnu_cpp11_abi": [0, 1],
    }
    # default_options = ("shared=False", "gnu_cpp11_abi=0", "*:shared=False", "*:fPIC=True")
    default_options = ("shared=True", "gnu_cpp11_abi=0", "*:shared=True", "*:fPIC=True")
    generators = "cmake"
    exports_sources = "src/*"
    keep_imports = True
    requires_base_names = (
        'boost/1.68.0@conan/stable',
        'gtest/1.8.0@bincrafters/stable',
        'OpenSSL/1.1.0g@conan/stable',
    )
    requires_override_names = (
        'zlib/1.2.8@conan/stable',
    )
    # FIXME: build_requires = 'qibuild'

    # To create explicit dependancies instead of build dependancies, uncomment the line below and comment the build_requires one
    # requires = tuple((ref, 'override') for ref in requires_base_names) + requires_base_names
    build_requires = requires_base_names + requires_override_names

    def configure(self):
        pass

    def build(self):
        pass

    def imports(self):
        self.copy("*.h")
        self.copy("*.hpp")
        self.copy("*.ipp")
        self.copy("include/*")
        self.copy("*.lib", "", "lib")
        self.copy("*.dll", "", "bin")
        self.copy("*.dylib*", "", "lib")
        self.copy("*.so*", "", "lib")
        self.copy("*.a", "", "lib")

    def package(self):
        self.copy("*.h")
        self.copy("*.hpp")
        self.copy("*.ipp")
        self.copy("include/*")
        self.copy("*.lib", dst="lib", keep_path=False)
        self.copy("*.dll", dst="bin", keep_path=False)
        self.copy("*.dylib*", dst="lib", keep_path=False)
        self.copy("*.so*", dst="lib", keep_path=False)
        self.copy("*.a", dst="lib", keep_path=False)

    def package_info(self):
        # Note: This list is exhaustive at package time as we have the list og build_dependencies
        # but when a consumer will ask for a conan install, this list will reduce to the hardcoded names
        # The sideeffect is the CONAN_LIBS var in conanbuildinfo.cmake will be quite empty and the consumer
        # will need to explicitly add the wanted target libraries to link in its CMakeList.txt
        #
        # e.g. add some lines like below in CMakeList.txt:
        # set(SPEC_LIBS boost_system boost_atomic boost_date_time boost_filesystem boost_locale boost_thread boost_chrono boost_regex boost_program_options crypto ssl)
        # add_executable(example example.cpp)
        # target_link_libraries(example ${CONAN_LIBS} ${SPEC_LIBS}) # Intead of target_link_libraries(example ${CONAN_LIBS})
        for dep_name in self.deps_cpp_info.deps:
            self.cpp_info.libs.extend(self.deps_cpp_info[dep_name].libs)

        for dep_name in ["dl", "pthread"]:
            if dep_name not in self.cpp_info.libs:
                self.cpp_info.libs.append(dep_name)

        self.output.info("REPACKAGED DEPS : %s" % self.cpp_info.libs)
