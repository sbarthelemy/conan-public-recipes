from conans import ConanFile, CMake, tools


class LibAlmathConan(ConanFile):
    name = "libalmath"
    version = "0.1"
    license = "BSD-3-Clause"
    url = "https://gitlab.com/sbarthelemy/conan-public-recipes"
    description = "Math library to work with the Motion module from NAOqi"
    settings = "os", "compiler", "build_type", "arch", "cppstd"
    options = {"shared": [True, False]}
    default_options = "shared=False"
    generators = "cmake"
    scm = {
        'type': 'git',
        'url': 'https://github.com/aldebaran/libalmath.git',
        'subfolder': 'libalmath',
        'revision': 'release-2.9',
    }
    requires = (
        'libqi/1.6.3@sbre/testing',
        'qilang/0.1@sbre/testing',
        'boost/1.68.0@conan/stable',
        'gtest/1.8.0@bincrafters/stable',
        'eigen/3.3.5@conan/stable',
        )
    # FIXME: build_requires = 'qibuild'

    def source(self):
        # This might be useful to guarantee proper /MT /MD linkage in MSVC
        # if the packaged project doesn't have variables to set it properly
        tools.replace_in_file(
            'libalmath/CMakeLists.txt',
            'project(ALMATH)',
            (
                'project(ALMATH)\n'
                'include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)\n'
                'conan_basic_setup()\n'
            )
        )

    def configure(self):
        # Won't build unless we force the C++ standard
        if self.settings.compiler != 'Visual Studio':
            self.settings.cppstd = 'gnu11'

    def build(self):
        cmake = CMake(self, parallel=False)
        cmake.definitions['ALMATH_WITH_QIGEOMETRY'] = False
        cmake.definitions['WITH_PYTHON'] = False
        cmake.verbose = True
        cmake.configure(source_folder="libalmath")
        cmake.build()
        cmake.install()

    def package_info(self):
        self.cpp_info.libs = ["almath"]
